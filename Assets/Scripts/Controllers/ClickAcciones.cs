﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Android;

public class ClickAcciones : MonoBehaviour
{
    //Modal General
    public GameObject spriteModal;
    //Modal Especificos
    public GameObject modalCompra, modalVenta, modalPOpciones, modalPBanco, modalPSocio,modalAviso;
    public List<Sprite> imagenesMateria;
    public Image iconoFicha;
    public GameObject btnAtras, btnSiguiente;
    public Text txtTitulo,txtCantidad,txtAviso;
    public GameObject btnAleatorio;

    public Button btnVender, btnComprar, btnPrestar, btnSeguir;

    //Variables de jugador
    int indiceJugador,numeroMateria,costoMateria,indexItem=1;

    //Variable casillero
    public static string casillero="Inicio";

    //Variables de estado
    public static bool ExisteDeuda = false;
    public static int MontoDeuda = 0;


    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        Castigo_Recompensa();
    }



    private void DarCastigo(int monto,int indexJugador)
    {
        if (ValidacionCampos.listaJugadores[indexJugador].DineroActual >= monto)
        {
            ValidacionCampos.listaJugadores[indiceJugador].DineroActual = ValidacionCampos.listaJugadores[indiceJugador].DineroActual - monto;
            if (monto==20)
            {
                StartCoroutine(MostrarAviso(modalAviso,txtAviso, "Pagas 20 por publicidad en tus redes"));
            }
            else if (monto==50)
            {
                StartCoroutine(MostrarAviso(modalAviso,txtAviso, "Pagas 50 para el sueldo de tus empleados"));
            }
            else if (monto == 100)
            {
                StartCoroutine(MostrarAviso(modalAviso,txtAviso, "Pagas 100 en tributos"));
            }
        }
        else
        {
            AbrirModal("Prestar");
            ExisteDeuda = true;
            MontoDeuda = monto;
            StartCoroutine(MostrarAviso(modalAviso,txtAviso, "No te alcanza para pagar. Necesitas un préstamo"));
        }
    }

    void Castigo_Recompensa()
    {
        //ReconocerJugador();
        if (casillero=="Castigo20")
        {
            ReconocerJugador();
            DarCastigo(20, indiceJugador);
            casillero = "";
        }
        else if (casillero=="Castigo50")
        {
            ReconocerJugador();
            DarCastigo(50, indiceJugador);
            casillero = "";
        }
        else if (casillero == "Castigo100")
        {
            ReconocerJugador();
            DarCastigo(100, indiceJugador);
            casillero = "";
        }
        else if (casillero == "Recompensa20")
        {
            ReconocerJugador();
            ValidacionCampos.listaJugadores[indiceJugador].DineroActual = ValidacionCampos.listaJugadores[indiceJugador].DineroActual + 20;
            StartCoroutine(MostrarAviso(modalAviso,txtAviso, "Ganas 20 en una incubadora de negocios"));
            casillero = "";
        }
        else if (casillero == "Recompensa30")
        {
            ReconocerJugador();
            ValidacionCampos.listaJugadores[indiceJugador].DineroActual = ValidacionCampos.listaJugadores[indiceJugador].DineroActual + 30;
            StartCoroutine(MostrarAviso(modalAviso, txtAviso, "Ganas 30 en un fondo concursable"));
            casillero = "";
        }
        else if (casillero == "Recompensa50")
        {
            ReconocerJugador();
            ValidacionCampos.listaJugadores[indiceJugador].DineroActual = ValidacionCampos.listaJugadores[indiceJugador].DineroActual + 50;
            StartCoroutine(MostrarAviso(modalAviso, txtAviso, "Ganas 50 por dar una charla de emprendimiento"));
            casillero = "";
        }
    }

    void ReconocerJugador()
    {
        for (int i=0;i<ValidacionCampos.cantidadJugadores;i++)
        {
            if (ValidacionCampos.listaJugadores[i].Turno)
            {
                indiceJugador = i;
                break;
            }
        }
    }

    int DarCosto()
    {
        int costo = 50;
        if (casillero == "MateriaPrimaTodos")
        {
            btnAtras.SetActive(true);
            btnSiguiente.SetActive(true);
            txtTitulo.text = "Leche";
            numeroMateria = 1;
            iconoFicha.sprite = imagenesMateria[0];
            costo = 60;
        }
        else
        {
            btnAtras.SetActive(false);
            btnSiguiente.SetActive(false);
            if (casillero == "MateriaPrima1")
            {
                txtTitulo.text = "Leche";
                numeroMateria = 1;
                iconoFicha.sprite = imagenesMateria[0];
            }
            else if (casillero == "MateriaPrima2")
            {
                txtTitulo.text = "Huevo";
                numeroMateria = 2;
                iconoFicha.sprite = imagenesMateria[1];
            }
            else if (casillero == "MateriaPrima3")
            {
                txtTitulo.text = "Fruta";
                numeroMateria = 3;
                iconoFicha.sprite = imagenesMateria[2];
            }
            else if (casillero == "MateriaPrima4")
            {
                txtTitulo.text = "Harina";
                numeroMateria = 4;
                iconoFicha.sprite = imagenesMateria[3];
            }

        }
        //Debug.Log(numeroMateria+" y su texto es: "+casillero);
        return costo;
        
    }

    public void AbrirModal(string nombreModal)
    {
        
        spriteModal.SetActive(true);
        btnVender.enabled = false;
        btnSeguir.enabled = false;
        btnComprar.enabled = false;
        btnPrestar.enabled = false;
        if (nombreModal=="Comprar")
        {
            costoMateria = DarCosto();
            modalCompra.SetActive(true);
        }
        else if (nombreModal == "Vender")
        {
            ReconocerJugador();
            if (ValidacionCampos.listaJugadores[indiceJugador].Cartas.Count>0)
            {
                GetComponent<ActualizarUI>().CambiarItemProducto("");
                modalVenta.SetActive(true);
            }
            else
            {
                StartCoroutine(MostrarAviso(modalAviso, txtAviso,"No tienes productos para vender"));
            }
        }
        else if (nombreModal=="Prestar")
        {
            modalPOpciones.SetActive(true);
        }
    }


    public void CerrarModal()
    {
        spriteModal.SetActive(false);
        modalCompra.SetActive(false);
        modalVenta.SetActive(false);
        modalPOpciones.SetActive(false);
        modalPSocio.SetActive(false);
        modalPBanco.SetActive(false);

        btnVender.enabled = true;
        btnSeguir.enabled = true;
        btnComprar.enabled = true;
        btnPrestar.enabled = true;

        txtCantidad.text = "0";
    }

    public void PrestamoOpcionElgida(string opcion)
    {
        if (opcion=="banco")
        {
            ReconocerJugador();
            if (ValidacionCampos.listaJugadores[indiceJugador].NumeroTarjetasPrestamoBanco<=3)
            {
                modalPOpciones.SetActive(false);
                modalPBanco.SetActive(true);
                GetComponent<OnClick>().SonidoBoton();
                Debug.Log("Llevas: "+ ValidacionCampos.listaJugadores[indiceJugador].NumeroTarjetasPrestamoBanco+" prestamos");
            }
            else
            {
                StartCoroutine(MostrarAviso(modalAviso, txtAviso,"Su número de préstamos bancarios ya se agotó"));
                GetComponent<ActualizarUI>().AvisoSonido();
            }
            
        }
        else
        {
            ReconocerJugador();
            if (ValidacionCampos.listaJugadores[indiceJugador].NumeroTarjetasPrestamoSocio < 2) 
            {
                modalPOpciones.SetActive(false);
                modalPSocio.SetActive(true);
                GetComponent<OnClick>().SonidoBoton();
            }
            else
            {
                StartCoroutine(MostrarAviso(modalAviso, txtAviso, "Su número de préstamos de socios ya se agotó"));
                GetComponent<ActualizarUI>().AvisoSonido();
            }
            
        }
        
    }

    public void RegresarAOpcion()
    {
        modalPOpciones.SetActive(true);
        modalPBanco.SetActive(false);
        modalPSocio.SetActive(false);
        GetComponent<OnClick>().SonidoBoton();
    }
    public void AlterarCantidad(string operacion)
    {
        int cantidad=int.Parse(txtCantidad.text);
        if (operacion=="sumar")
        {
            cantidad++;
        }
        else if (operacion=="restar")
        {
            if (cantidad<=0)
            {
                cantidad = 0;
            }
            else
            {
                cantidad--;
            }
        }
        Debug.Log(operacion + " este numero:" + cantidad);
        txtCantidad.text = cantidad.ToString();
        GetComponent<OnClick>().SonidoBoton();
    }

    public void Comprar()
    {
        ReconocerJugador();
        //modalCompra.SetActive(true);
        float costo=costoMateria;
        int cantidad =int.Parse(txtCantidad.text);
        string nombreMateria = "";
        if (ValidarCompra(ValidacionCampos.listaJugadores[indiceJugador].DineroActual,costo,cantidad))
        {
            ValidacionCampos.listaJugadores[indiceJugador].DineroActual = ValidacionCampos.listaJugadores[indiceJugador].DineroActual - (costo * cantidad);

            if (numeroMateria == 1)
            {
                ValidacionCampos.listaJugadores[indiceJugador].CantidadMaterial1 = ValidacionCampos.listaJugadores[indiceJugador].CantidadMaterial1 + cantidad;
                nombreMateria = "Leche";
            }
            else if (numeroMateria == 2)
            {
                ValidacionCampos.listaJugadores[indiceJugador].CantidadMaterial2 = ValidacionCampos.listaJugadores[indiceJugador].CantidadMaterial2 + cantidad;
                nombreMateria = "Huevo";
            }
            else if (numeroMateria == 3)
            {
                ValidacionCampos.listaJugadores[indiceJugador].CantidadMaterial3 = ValidacionCampos.listaJugadores[indiceJugador].CantidadMaterial3 + cantidad;
                nombreMateria = "Fruta";
            }
            else if (numeroMateria == 4)
            {
                ValidacionCampos.listaJugadores[indiceJugador].CantidadMaterial4 = ValidacionCampos.listaJugadores[indiceJugador].CantidadMaterial4 + cantidad;
                nombreMateria = "Harina";
            }
            if (cantidad==0)
            {
                StartCoroutine(MostrarAviso(modalAviso, txtAviso, "Debes comprar mínimo 1 materia"));
                GetComponent<ActualizarUI>().AvisoSonido();
            }
            else
            {
                StartCoroutine(MostrarAviso(modalAviso, txtAviso, "Haz comprado " + cantidad + " materias de " + nombreMateria));
                GetComponent<OnClick>().SonidoDinero();
            }
            
            if (costo==50)
            {
                CerrarModal();
            }
        }
        else
        {
            
            StartCoroutine(MostrarAviso(modalAviso, txtAviso,"No te alcanza. Ada te recomienda un préstamo"));
            GetComponent<ActualizarUI>().AvisoSonido();
        }
        

    }

    private bool ValidarCompra(float dineroActual,float costoUnitario,int cantidad)
    {
        bool validado=false;

        if (dineroActual>=(costoUnitario * cantidad))
        {
            validado = true;
        }
        else
        {
            validado = false;
        }

        return validado;
    }

    public void CambiarItem(string opcion)
    {
        if (opcion=="next")
        {
            indexItem++;
            if (indexItem == 5)
            {
                indexItem = 1;
            }
        }
        else if (opcion == "back")
        {
            indexItem--;
            if (indexItem == 0)
            {
                indexItem = 4;
            }
        }

        if (indexItem == 1)
        {
            txtTitulo.text = "Leche";
            numeroMateria = 1;
            iconoFicha.sprite = imagenesMateria[0];
        }
        else if (indexItem == 2)
        {
            txtTitulo.text = "Huevo";
            numeroMateria = 2;
            iconoFicha.sprite = imagenesMateria[1];
        }
        else if (indexItem == 3)
        {
            txtTitulo.text = "Fruta";
            numeroMateria = 3;
            iconoFicha.sprite = imagenesMateria[2];
        }
        else if (indexItem == 4)
        {
            txtTitulo.text = "Harina";
            numeroMateria = 4;
            iconoFicha.sprite = imagenesMateria[3];
        }

        GetComponent<OnClick>().SonidoBoton();

    }

    IEnumerator MostrarAviso(GameObject modal, Text goAviso,string txtAviso)
    {
        modal.SetActive(true);
        goAviso.text = txtAviso;
        yield return new WaitForSeconds(3);
        modal.SetActive(false);
    }

    public void AumentarPrestamo(Text txtCantidad)
    {
        int cantidad=int.Parse(txtCantidad.text);
        cantidad = cantidad + 100;
        ReconocerJugador();
        if (ValidacionCampos.listaJugadores[indiceJugador].NumeroTarjetasPrestamoBanco==0)
        {
            if (cantidad == 500)
            {
                cantidad = 400;
                GetComponent<ActualizarUI>().AvisoSonido();
                StartCoroutine(MostrarAviso(modalAviso, txtAviso, "El préstamo máximo disponible es 400"));
            }
            else
            {
                GetComponent<OnClick>().SonidoBoton();
            }
        }
        else if (ValidacionCampos.listaJugadores[indiceJugador].NumeroTarjetasPrestamoBanco == 1)
        {
            if (cantidad == 400)
            {
                cantidad = 300;
                StartCoroutine(MostrarAviso(modalAviso, txtAviso, "El préstamo máximo disponible es 300"));
                GetComponent<ActualizarUI>().AvisoSonido();
            }
            else
            {
                GetComponent<OnClick>().SonidoBoton();
            }
        }
        else if (ValidacionCampos.listaJugadores[indiceJugador].NumeroTarjetasPrestamoBanco == 2)
        {
            if (cantidad == 300)
            {
                cantidad = 200;
                StartCoroutine(MostrarAviso(modalAviso, txtAviso, "El préstamo máximo disponible es 200"));
                GetComponent<ActualizarUI>().AvisoSonido();
            }
            else
            {
                GetComponent<OnClick>().SonidoBoton();
            }
        }
        else if (ValidacionCampos.listaJugadores[indiceJugador].NumeroTarjetasPrestamoBanco == 3)
        {
            if (cantidad == 200)
            {
                cantidad = 100;
                StartCoroutine(MostrarAviso(modalAviso, txtAviso, "El préstamo máximo disponible es 100"));
                GetComponent<ActualizarUI>().AvisoSonido();
            }
            else
            {
                GetComponent<OnClick>().SonidoBoton();
            }
        }

        txtCantidad.text = cantidad.ToString();
    }

    public void DisminuirPrestamo(Text txtCantidad)
    {
        int cantidad = int.Parse(txtCantidad.text);
        cantidad = cantidad - 100;
        if (cantidad == 0)
        {
            cantidad = 100;
            StartCoroutine(MostrarAviso(modalAviso, txtAviso, "El préstamo mínimo disponible es 100"));
            GetComponent<ActualizarUI>().AvisoSonido();
        }
        else
        {
            GetComponent<OnClick>().SonidoBoton();
        }
        txtCantidad.text = cantidad.ToString();
    }

   
    public void Prestar(Text txtCantidad)
    {
        int monto = int.Parse(txtCantidad.text);
        int numeroTarjetas = monto / 100;
        ValidacionCampos.listaJugadores[indiceJugador].NumeroTarjetasPrestamoBanco = ValidacionCampos.listaJugadores[indiceJugador].NumeroTarjetasPrestamoBanco + numeroTarjetas;
        ValidacionCampos.listaJugadores[indiceJugador].DineroActual = ValidacionCampos.listaJugadores[indiceJugador].DineroActual+monto;
        ValidacionCampos.listaJugadores[indiceJugador].PrestamoBancario = ValidacionCampos.listaJugadores[indiceJugador].NumeroTarjetasPrestamoBanco * 100;
        CerrarModal();
        txtCantidad.text = "100";
        GetComponent<OnClick>().SonidoBoton();
        if (ExisteDeuda)
        {
            PagarDeuda(MontoDeuda);
            StartCoroutine(MostrarAviso(modalAviso, txtAviso, "Se pagó la deuda de " + MontoDeuda));
            MontoDeuda = 0;
            GetComponent<ActualizarUI>().CastigoSonido();
        }
        GetComponent<OnClick>().SonidoDinero();
    }

    public void SocioAleatorio(Text txtCantidad)
    {
        btnAleatorio.SetActive(false);
        txtCantidad.gameObject.SetActive(true);
        //for (int i=1;i<6;i++)
        //{
        //    StartCoroutine(animacionAleatoriedad(txtCantidad,i*100));
        //}
        int montoAleatorio = Random.Range(1,7);
        PrestarSocio(montoAleatorio*100);
        txtCantidad.text =(montoAleatorio*100).ToString();
        GetComponent<OnClick>().SonidoBoton();
        if (ExisteDeuda)
        {
            PagarDeuda(MontoDeuda);
            StartCoroutine(MostrarAviso(modalAviso, txtAviso,"Se pagó la deuda de "+MontoDeuda));
            GetComponent<ActualizarUI>().CastigoSonido();
            MontoDeuda = 0;
        }
        GetComponent<OnClick>().SonidoDinero();
    }
    //Memo: ver cuandp sale error del index y terminar venta de cliente
    void PagarDeuda(int monto)
    {
        ReconocerJugador();
        ValidacionCampos.listaJugadores[indiceJugador].DineroActual = ValidacionCampos.listaJugadores[indiceJugador].DineroActual -
        monto;
        ExisteDeuda = false;
        
    }
    public void PrestarSocio(int monto)
    {
        ReconocerJugador();
        ValidacionCampos.listaJugadores[indiceJugador].DineroActual = ValidacionCampos.listaJugadores[indiceJugador].DineroActual + 
        monto;
        ValidacionCampos.listaJugadores[indiceJugador].NumeroTarjetasPrestamoSocio = ValidacionCampos.listaJugadores[indiceJugador].NumeroTarjetasPrestamoSocio + 1;
        ValidacionCampos.listaJugadores[indiceJugador].PrestamoSocio = ValidacionCampos.listaJugadores[indiceJugador].PrestamoSocio + monto;
        
    }

    public void CerrarModalSocio(Text txtMonto)
    {
        modalPSocio.SetActive(false);
        modalPOpciones.gameObject.SetActive(true);
        txtMonto.text = "0";
        txtMonto.gameObject.SetActive(false);
        btnAleatorio.gameObject.SetActive(true);
        GetComponent<OnClick>().SonidoBoton();
    }


    IEnumerator animacionAleatoriedad(Text txtCantidad,int valor)
    {
        txtCantidad.text = valor.ToString();
        valor = valor + 100;
        if (valor==700)
        {
            valor = 100;
        }
        yield return new WaitForSeconds(5);
        txtCantidad.text = valor.ToString();
        
    }

    

}
