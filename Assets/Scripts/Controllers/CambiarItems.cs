﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CambiarItems : MonoBehaviour
{
    // Start is called before the first frame update
    private int indexItem;
    public List<GameObject> gridInstrucciones;
    public void CambiarItem(string opcion)
    {
        if (opcion == "next")
        {
            indexItem++;
            if (indexItem == 4)
            {
                indexItem = 0;
                gridInstrucciones[indexItem].gameObject.SetActive(true);
                gridInstrucciones[3].gameObject.SetActive(false);
            }
            else
            {
                gridInstrucciones[indexItem].gameObject.SetActive(true);
                gridInstrucciones[indexItem - 1].gameObject.SetActive(false);
            }
        }
        else if (opcion == "back")
        {
            indexItem--;
            if (indexItem == -1)
            {
                indexItem = 3;
                gridInstrucciones[indexItem].gameObject.SetActive(true);
                gridInstrucciones[0].gameObject.SetActive(false);

            }
            else
            {
                gridInstrucciones[indexItem].gameObject.SetActive(true);
                gridInstrucciones[indexItem + 1].gameObject.SetActive(false);
            }
        }

        

        

        GetComponent<OnClick>().SonidoBoton();

    }
    void Start()
    {
        indexItem = 0;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
