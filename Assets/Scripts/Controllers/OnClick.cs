﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class OnClick : MonoBehaviour
{
    public AudioSource butonAudioSource;
    //public AudioSource fondoAudioSource;

    public AudioClip butonAudioClip,dineroClip;
    //public AudioClip avisoAudioClip;
    //public AudioClip pointAudioClip;
    //public AudioClip castigoAudioClip;

    public static string mensajeAcceso=null;

    
    private void Start()
    {
        
            
    }

    public void SonidoBoton()
    {
        butonAudioSource.clip = butonAudioClip;
        butonAudioSource.Play();
    }


    public void OtroSonido(AudioClip audio)
    {
        butonAudioSource.clip = audio;
        butonAudioSource.Play();
    }

    public void SonidoDinero()
    {
        butonAudioSource.clip = dineroClip;
        butonAudioSource.Play();
    }

    public void CambiarEscena(string NombreEscena)
    {
        SonidoBoton();
        SceneManager.LoadScene(NombreEscena);
        
    }

    public void IrAWeb(string url)
    {
        Application.OpenURL(url);
        SonidoBoton();
    }

    public void CerrarDialog(Image AlertDialog)
    {
        AlertDialog.gameObject.SetActive(false);
    }

    public void VolverAJugar()
    {
        BorrarJugadores();
        ClickAcciones.casillero = "";
        ClickAcciones.ExisteDeuda = false;
        ClickAcciones.MontoDeuda = 0;
        OnClick.mensajeAcceso = null;
        SonidoBoton();
        SceneManager.LoadScene("CrearPartida");
        
    }

    public void Valorar()
    {
        BorrarJugadores();
        ClickAcciones.casillero = "";
        ClickAcciones.ExisteDeuda = false;
        ClickAcciones.MontoDeuda = 0;
        OnClick.mensajeAcceso = null;
        SonidoBoton();
        SceneManager.LoadScene("Valoracion");
        
    }

    private void BorrarJugadores()
    {
        for (int i = 0; i <= ValidacionCampos.listaJugadores.Count; i++)
        {
            i = 0;
            ValidacionCampos.listaJugadores.RemoveAt(0);
        }

    }

    




}
