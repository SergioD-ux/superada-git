﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Assets.Scripts.Views;

namespace Assets.Scripts.Controllers
{
    public class ControllerReloj : MonoBehaviour
    {
        public void Cronometro(float tiempoEsperado)
        {

            //float tiempoRealEscena;
            //tiempoRealEscena = Time.realtimeSinceStartup; /*- tiempoInicialEscena;*/
            int minutos, segundos;

            //float tiempoTranscurrido = tiempoRealEscena;

            float tiempoTranscurrido = Time.timeSinceLevelLoad;

            //Debug.Log(tiempoTranscurrido + "y" + Time.timeSinceLevelLoad);
            //tiempoEsperado = tiempoEsperado + (tiempoRealEscena - tiempoTranscurrido); 
            float tiempoAMostrar = tiempoEsperado - tiempoTranscurrido;
            //Debug.Log("Es:"+tiempoAMostrar);

            minutos = (int)tiempoAMostrar / 60;
            segundos = (int)tiempoAMostrar % 60;
            //Debug.Log(minutos.ToString("00") + ":" + segundos.ToString("00"));
            txtTiempo.text = minutos.ToString("00") + ":" + segundos.ToString("00");


            if (segundos < 0)
            {
                minutos = 0;
                segundos = 0;
                gameOver = true;
                SceneManager.LoadScene("Resultados");
            }

        }
    }
}
