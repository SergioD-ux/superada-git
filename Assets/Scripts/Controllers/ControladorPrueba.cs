﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;

public class ControladorPrueba : MonoBehaviour
{
    // Start is called before the first frame update
    TimeSpan timeRemaining;
    public static bool pruebaFinalizada = false;
    public Button btnJugar;
    public Text textoPrueba;

    void Start()
    {
        int dia = DateTime.Today.Day;
        int mes = DateTime.Today.Month;
        int anio = DateTime.Today.Year;

        Debug.Log(dia+"/"+mes + "/" + anio);

        if (dia<=30 && mes==4 && anio==2020)
        {
            pruebaFinalizada = false;
            textoPrueba.gameObject.SetActive(false);
        }
        else
        {
            pruebaFinalizada = true;
            btnJugar.gameObject.SetActive(false);
            textoPrueba.gameObject.SetActive(true);
        }
    }

    // Update is called once per frame
    void Update()
    {
        DiasdePrueba();
    }
    void DiasdePrueba()
    {
    }
}
