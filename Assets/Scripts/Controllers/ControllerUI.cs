﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Assets.Scripts.Models;

namespace Assets.Scripts.Controllers
{
    public class ControllerUI : MonoBehaviour
    {
        //Modañ Venta
        int indexItem = 1; //Item Producto
        
        int numeroMateria;

        public static List<CartaProducto> listaCartaProductos = new List<CartaProducto>();

        //Variable de estado
        public static bool cartaAsignada = false;

        //Variable Jugador
        public int indiceJugador;


        public static void ListarProductos(int indiceJugador, List<Image> espacioProdcutos, List<Sprite> imagenesProductos)
        {
            int cantidadCartas = ValidacionCampos.listaJugadores[indiceJugador].Cartas.Count;
            espacioProdcutos[0].sprite = null;
            espacioProdcutos[1].sprite = null;
            espacioProdcutos[2].sprite = null;
            espacioProdcutos[3].sprite = null;
            espacioProdcutos[4].sprite = null;

            espacioProdcutos[0].color = Color.white;
            espacioProdcutos[1].color = Color.white;
            espacioProdcutos[2].color = Color.white;
            espacioProdcutos[3].color = Color.white;
            espacioProdcutos[4].color = Color.white;

            for (int i = 0; i < cantidadCartas; i++)
            {
                espacioProdcutos[i].sprite = imagenesProductos[(ValidacionCampos.listaJugadores[indiceJugador].Cartas[i].IdentificadorMateria / 10) - 1];
                if (!PuedeEstaVender(indiceJugador, i))
                {
                    espacioProdcutos[i].color = Color.gray;
                }
                else
                {
                    espacioProdcutos[i].color = Color.white;
                }
            }
        }
        public static bool PuedeEstaVender(int indiceJugador, int indiceCarta)
        {

            bool respuesta = false;
            int cantidadCartas = ValidacionCampos.listaJugadores[indiceJugador].Cartas.Count;
            string materia1 = ValidacionCampos.listaJugadores[indiceJugador].Cartas[indiceCarta].Materia_uno;
            string materia2 = ValidacionCampos.listaJugadores[indiceJugador].Cartas[indiceCarta].Materia_dos;
            int cantidadLeche = ValidacionCampos.listaJugadores[indiceJugador].CantidadMaterial1;
            int cantidadHuevo = ValidacionCampos.listaJugadores[indiceJugador].CantidadMaterial2;
            int cantidadFruta = ValidacionCampos.listaJugadores[indiceJugador].CantidadMaterial3;
            int cantidadHarina = ValidacionCampos.listaJugadores[indiceJugador].CantidadMaterial4;

            if (materia1 == "Leche" && cantidadLeche > 0)
            {
                if (materia2 == "Leche" && cantidadLeche > 1)
                {

                    respuesta = true;
                }
                else if (materia2 == "Huevo" && cantidadHuevo > 0)
                {
                    respuesta = true;
                }
                else if (materia2 == "Fruta" && cantidadFruta > 0)
                {
                    respuesta = true;
                }
                else if (materia2 == "Harina" && cantidadHarina > 0)
                {
                    respuesta = true;
                }
            }
            //-----------------------------------------------------
            else if (materia1 == "Huevo" && cantidadHuevo > 0)
            {
                if (materia2 == "Leche" && cantidadLeche > 0)
                {

                    respuesta = true;
                }
                else if (materia2 == "Huevo" && cantidadHuevo > 1)
                {
                    respuesta = true;
                }
                else if (materia2 == "Fruta" && cantidadFruta > 0)
                {
                    respuesta = true;
                }
                else if (materia2 == "Harina" && cantidadHarina > 0)
                {
                    respuesta = true;
                }
            }
            //-------------------------------------------
            else if (materia1 == "Fruta" && cantidadFruta > 0)
            {
                if (materia2 == "Leche" && cantidadLeche > 0)
                {

                    respuesta = true;
                }
                else if (materia2 == "Huevo" && cantidadHuevo > 0)
                {
                    respuesta = true;
                }
                else if (materia2 == "Fruta" && cantidadFruta > 1)
                {
                    respuesta = true;
                }
                else if (materia2 == "Harina" && cantidadHarina > 0)
                {
                    respuesta = true;
                }
            }
            //-----------------------------------------------------
            else if (materia1 == "Harina" && cantidadHarina > 0)
            {
                if (materia2 == "Leche" && cantidadLeche > 0)
                {

                    respuesta = true;
                }
                else if (materia2 == "Huevo" && cantidadHuevo > 0)
                {
                    respuesta = true;
                }
                else if (materia2 == "Fruta" && cantidadFruta > 0)
                {
                    respuesta = true;
                }
                else if (materia2 == "Harina" && cantidadHarina > 1)
                {
                    respuesta = true;
                }
            }
            return respuesta;
        }


        public static CartaProducto DarCarta(int indiceCarta)
        {
            CartaProducto carta = new CartaProducto();
            carta.IdentificadorMateria = listaCartaProductos[indiceCarta].IdentificadorMateria;
            carta.IdentificadorIndice = listaCartaProductos[indiceCarta].IdentificadorIndice;
            carta.Nombre = listaCartaProductos[indiceCarta].Nombre;
            carta.Materia_uno = listaCartaProductos[indiceCarta].Materia_uno;
            carta.Materia_dos = listaCartaProductos[indiceCarta].Materia_dos;
            carta.Monto = listaCartaProductos[indiceCarta].Monto;
            carta.Ocupado = true;
            carta.Disponible = true;
            return carta;
        }

        public static void BucleProducto(CartaProducto cartaProducto, int iProducto, string nombre, string materia1, string materia2, float monto)
        {
            for (int i = 1; i < 6; i++)
            {
                cartaProducto.IdentificadorMateria = iProducto;
                cartaProducto.IdentificadorIndice = i;
                cartaProducto.Nombre = nombre;
                cartaProducto.Materia_uno = materia1;
                cartaProducto.Materia_dos = materia2;
                cartaProducto.Monto = monto;
                cartaProducto.Ocupado = false;
                cartaProducto.Disponible = true;

                listaCartaProductos.Add(cartaProducto);

            }
        }

        public static bool VerificarVenta(int indiceCarta)
        {
            bool respuesta = false;
            string materia1 = ValidacionCampos.listaJugadores[indiceJugador].Cartas[indiceCarta].Materia_uno;
            string materia2 = ValidacionCampos.listaJugadores[indiceJugador].Cartas[indiceCarta].Materia_dos;
            int cantidadLeche = ValidacionCampos.listaJugadores[indiceJugador].CantidadMaterial1;
            int cantidadHuevo = ValidacionCampos.listaJugadores[indiceJugador].CantidadMaterial2;
            int cantidadFruta = ValidacionCampos.listaJugadores[indiceJugador].CantidadMaterial3;
            int cantidadHarina = ValidacionCampos.listaJugadores[indiceJugador].CantidadMaterial4;

            if (materia1 == "Leche" && cantidadLeche > 0)
            {
                if (materia2 == "Leche" && cantidadLeche > 1)
                {
                    ValidacionCampos.listaJugadores[indiceJugador].CantidadMaterial1 = cantidadLeche - 2;
                    respuesta = true;
                }
                else if (materia2 == "Huevo" && cantidadHuevo > 0)
                {
                    ValidacionCampos.listaJugadores[indiceJugador].CantidadMaterial1 = cantidadLeche - 1;
                    ValidacionCampos.listaJugadores[indiceJugador].CantidadMaterial2 = cantidadHuevo - 1;
                    respuesta = true;
                }
                else if (materia2 == "Fruta" && cantidadFruta > 0)
                {
                    ValidacionCampos.listaJugadores[indiceJugador].CantidadMaterial1 = cantidadLeche - 1;
                    ValidacionCampos.listaJugadores[indiceJugador].CantidadMaterial3 = cantidadFruta - 1;
                    respuesta = true;
                }
                else if (materia2 == "Harina" && cantidadHarina > 0)
                {
                    ValidacionCampos.listaJugadores[indiceJugador].CantidadMaterial1 = cantidadLeche - 1;
                    ValidacionCampos.listaJugadores[indiceJugador].CantidadMaterial4 = cantidadHarina - 1;
                    respuesta = true;
                }
            }
            //-----------------------------------------------------
            else if (materia1 == "Huevo" && cantidadHuevo > 0)
            {
                if (materia2 == "Leche" && cantidadLeche > 0)
                {
                    ValidacionCampos.listaJugadores[indiceJugador].CantidadMaterial1 = cantidadLeche - 1;
                    ValidacionCampos.listaJugadores[indiceJugador].CantidadMaterial2 = cantidadHuevo - 1;
                    respuesta = true;
                }
                else if (materia2 == "Huevo" && cantidadHuevo > 1)
                {
                    ValidacionCampos.listaJugadores[indiceJugador].CantidadMaterial2 = cantidadHuevo - 2;
                    respuesta = true;
                }
                else if (materia2 == "Fruta" && cantidadFruta > 0)
                {
                    ValidacionCampos.listaJugadores[indiceJugador].CantidadMaterial3 = cantidadFruta - 1;
                    ValidacionCampos.listaJugadores[indiceJugador].CantidadMaterial2 = cantidadHuevo - 1;
                    respuesta = true;
                }
                else if (materia2 == "Harina" && cantidadHarina > 0)
                {
                    ValidacionCampos.listaJugadores[indiceJugador].CantidadMaterial4 = cantidadHarina - 1;
                    ValidacionCampos.listaJugadores[indiceJugador].CantidadMaterial2 = cantidadHuevo - 1;
                    respuesta = true;
                }
            }
            //-------------------------------------------
            else if (materia1 == "Fruta" && cantidadFruta > 0)
            {
                if (materia2 == "Leche" && cantidadLeche > 0)
                {
                    ValidacionCampos.listaJugadores[indiceJugador].CantidadMaterial1 = cantidadLeche - 1;
                    ValidacionCampos.listaJugadores[indiceJugador].CantidadMaterial3 = cantidadFruta - 1;
                    respuesta = true;
                }
                else if (materia2 == "Huevo" && cantidadHuevo > 0)
                {
                    ValidacionCampos.listaJugadores[indiceJugador].CantidadMaterial2 = cantidadHuevo - 1;
                    ValidacionCampos.listaJugadores[indiceJugador].CantidadMaterial3 = cantidadFruta - 1;
                    respuesta = true;
                }
                else if (materia2 == "Fruta" && cantidadFruta > 1)
                {
                    ValidacionCampos.listaJugadores[indiceJugador].CantidadMaterial3 = cantidadFruta - 2;
                    respuesta = true;
                }
                else if (materia2 == "Harina" && cantidadHarina > 0)
                {
                    ValidacionCampos.listaJugadores[indiceJugador].CantidadMaterial4 = cantidadHarina - 1;
                    ValidacionCampos.listaJugadores[indiceJugador].CantidadMaterial3 = cantidadFruta - 1;
                    respuesta = true;
                }
            }
            //-----------------------------------------------------
            else if (materia1 == "Harina" && cantidadHarina > 0)
            {
                if (materia2 == "Leche" && cantidadLeche > 0)
                {
                    ValidacionCampos.listaJugadores[indiceJugador].CantidadMaterial4 = cantidadHarina - 1;
                    ValidacionCampos.listaJugadores[indiceJugador].CantidadMaterial1 = cantidadLeche - 1;
                    respuesta = true;
                }
                else if (materia2 == "Huevo" && cantidadHuevo > 0)
                {
                    ValidacionCampos.listaJugadores[indiceJugador].CantidadMaterial4 = cantidadHarina - 1;
                    ValidacionCampos.listaJugadores[indiceJugador].CantidadMaterial2 = cantidadHuevo - 1;
                    respuesta = true;
                }
                else if (materia2 == "Fruta" && cantidadFruta > 0)
                {
                    ValidacionCampos.listaJugadores[indiceJugador].CantidadMaterial4 = cantidadHarina - 1;
                    ValidacionCampos.listaJugadores[indiceJugador].CantidadMaterial3 = cantidadFruta - 1;
                    respuesta = true;
                }
                else if (materia2 == "Harina" && cantidadHarina > 1)
                {
                    ValidacionCampos.listaJugadores[indiceJugador].CantidadMaterial4 = cantidadHarina - 2;
                    respuesta = true;
                }
            }
            return respuesta;
        }
    }

}