﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using Assets.Scripts.Controllers;

public class Reloj : MonoBehaviour
{
    // Start is called before the first frame update

    public Text txtTiempo;
    public static bool gameOver;
    public AudioSource audioFondo;
    public Button btnPausa;
    public Text txtFrase;
    public GameObject modalPausa,jugadores;
    public GameObject panel,modalAcciones;
    public int tiempoJuego;

    void Start()
    {
        
    }

    

    // Update is called once per frame
    void Update()
    {
        ControllerReloj.Cronometro(60*tiempoJuego);
    }


    public void Pause()
    {
        jugadores.SetActive(false);
        panel.SetActive(true);
        modalPausa.gameObject.SetActive(true);
        audioFondo.Pause();
        Time.timeScale = 0;
        

        //string[] frase = new string[5];
        //frase[0] = "Para poder tener ingresos, se debe invertir";
        //frase[1] = "Los préstamos nos puede ayudar como capital. Usalos sabiamente";
        //frase[2] = "Puedes comprar las materias que necesitas o comprar más materias y estar preparado para cualquier producto. ¿Qué estrategia usarás?";
        //frase[3] = "Satisfacer los productos de tus clientes genera buena relación con ellos";
        //frase[4] = "¿Sabías que con este juego aprendes a emprender con el Modelo de Negocio Canvas?";
        //frase[5] = "Quedate en casa jugando con Super Ada. No olvides lavarte las manos";


        //txtFrase.text = frase[Random.Range(0, 5)];
        btnPausa.gameObject.SetActive(false);
    }
    public void Reanudar()
    {
        jugadores.SetActive(true);
        modalPausa.gameObject.SetActive(false);
        panel.SetActive(false);
        audioFondo.UnPause();
        Time.timeScale = 1f;
        btnPausa.gameObject.SetActive(true);
    }

    public void Reiniciar(string nombreEscena)
    {
        Time.timeScale = 1f;
        ReinicarDatos();
        SceneManager.LoadScene(nombreEscena);
    }

    public void VolverMenu()
    {
        BorrarJugadores();
        ClickAcciones.casillero = "";
        ClickAcciones.ExisteDeuda = false;
        ClickAcciones.MontoDeuda = 0;
        OnClick.mensajeAcceso = null;
        Time.timeScale = 1f;
        SceneManager.LoadScene("CrearPartida");
    }

    private void BorrarJugadores()
    {
        for (int i=0; i <= ValidacionCampos.listaJugadores.Count; i++)
        {
            i = 0;
            ValidacionCampos.listaJugadores.RemoveAt(0);
        }
        
    }

    private void ReinicarDatos()
    {
        for (int i=0;i<ValidacionCampos.listaJugadores.Count;i++)
        {
            ValidacionCampos.listaJugadores[i].NumeroJugador = i + 1;
            ValidacionCampos.listaJugadores[i].DineroActual = 100;
            ValidacionCampos.listaJugadores[i].DineroGanado = 100;
            ValidacionCampos.listaJugadores[i].PrestamoBancario = 0;
            ValidacionCampos.listaJugadores[i].PrestamoBancarioDevolucionTotal = 0;
            ValidacionCampos.listaJugadores[i].PrestamoSocio = 0;
            ValidacionCampos.listaJugadores[i].PrestamoSocioDevolucionTotal = 0;
            ValidacionCampos.listaJugadores[i].Turno = false;
            ValidacionCampos.listaJugadores[i].Activado = true;
            ValidacionCampos.listaJugadores[i].Posicion = null;
            ValidacionCampos.listaJugadores[i].PosicionActual = 0;
            ValidacionCampos.listaJugadores[i].NumeroTarjetasCliente = 0;
            ValidacionCampos.listaJugadores[i].NumeroTarjetasPrestamoBanco = 0;
            ValidacionCampos.listaJugadores[i].NumeroTarjetasPrestamoSocio = 0;
            ValidacionCampos.listaJugadores[i].CantidadMaterial1 = 0;
            ValidacionCampos.listaJugadores[i].CantidadMaterial2 = 0;
            ValidacionCampos.listaJugadores[i].CantidadMaterial3 = 0;
            ValidacionCampos.listaJugadores[i].CantidadMaterial4 = 0;
            ValidacionCampos.listaJugadores[i].Cartas = null;
        }
        
    }

}
