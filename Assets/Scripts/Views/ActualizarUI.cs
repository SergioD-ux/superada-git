﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Assets.Scripts.Models;
using Assets.Scripts.Controllers;

namespace Assets.Scripts.Views
{
    public class ActualizarUI : MonoBehaviour
    {

        public Text txtNombre, txtDinero, txtPBancario, txtPSocio, txtCantMP1, txtCantMP2, txtCantMP3, txtCantMP4;
        public GameObject grigBotonesAcciones, modalAviso;
        public List<Sprite> cartasProductos;
        public List<Image> imagenProductos;
        public Image fichaJugadorDetalle;
        public List<Sprite> fichaJugador;
        public Text txtTitulo, txtPrecio, txtAviso;

        public GameObject btnVender;

        public List<Image> iconoMateria;
        public List<Sprite> imagenesMateria;


        //Sonido-Audio


        public AudioClip avisoAudioClip;
        public AudioClip puntoAudioClip;
        public AudioClip castigoAudioClip;


        // Start is called before the first frame update
        private void Start()
        {
            InstanciarCartas();
            AsignarPrimeraCarta();
            Actualizar();
            Debug.Log("Cantidad de cartas: " + ValidacionCampos.listaJugadores[0].Cartas.Count);
            Debug.Log("Cantidad de cartas: " + ValidacionCampos.listaJugadores[1].Cartas.Count);

        }
        private void Update()
        {
            Actualizar();
        }
        private void Actualizar()
        {
            for (int i = 0; i < ValidacionCampos.listaJugadores.Count; i++)
            {
                if (ValidacionCampos.listaJugadores[i].Turno)
                {
                    int indiceficha = ValidacionCampos.listaJugadores[i].NumeroJugador - 1;
                    txtNombre.text = ValidacionCampos.listaJugadores[i].Nombre;
                    fichaJugadorDetalle.sprite = fichaJugador[indiceficha];
                    txtDinero.text = ValidacionCampos.listaJugadores[i].DineroActual.ToString();
                    txtPBancario.text = ValidacionCampos.listaJugadores[i].PrestamoBancario.ToString();
                    txtPSocio.text = ValidacionCampos.listaJugadores[i].PrestamoSocio.ToString();
                    txtCantMP1.text = ValidacionCampos.listaJugadores[i].CantidadMaterial1.ToString();
                    txtCantMP2.text = ValidacionCampos.listaJugadores[i].CantidadMaterial2.ToString();
                    txtCantMP3.text = ValidacionCampos.listaJugadores[i].CantidadMaterial3.ToString();
                    txtCantMP4.text = ValidacionCampos.listaJugadores[i].CantidadMaterial4.ToString();
                    ControllerUI.ListarProductos(i, imagenProductos, cartasProductos);
                }
            }
            if (TirarDado.estadoTirarClick)
            {
                grigBotonesAcciones.SetActive(false);
            }
            else
            {
                grigBotonesAcciones.SetActive(true);
            }
        }

        private void AsignarPrimeraCarta()
        {

            for (int i = 0; i < ValidacionCampos.listaJugadores.Count; i++)
            {
                int numeroRandom = Random.Range(0, 35);
                if (ValidacionCampos.listaJugadores[i].Activado)
                {
                    ValidacionCampos.listaJugadores[i].Cartas = new List<CartaProducto>();
                    ValidacionCampos.listaJugadores[i].Cartas.Add(ControllerUI.DarCarta(numeroRandom));

                    Debug.Log(i + " le tocó " + ValidacionCampos.listaJugadores[i].Cartas[0].Nombre);
                    Debug.Log("Llegó aquí");
                }
            }
        }


        private void InstanciarCartas()
        {
            CartaProducto carta1Alfajor = new CartaProducto();
            CartaProducto carta2Flan = new CartaProducto();
            CartaProducto carta3Helado = new CartaProducto();
            CartaProducto carta4Jugo = new CartaProducto();
            CartaProducto carta5Pie = new CartaProducto();
            CartaProducto carta6Pionono = new CartaProducto();
            CartaProducto carta7Torta = new CartaProducto();

            ControllerUI.BucleProducto(carta1Alfajor, 10, "Alfajor", "Harina", "Harina", 120);
            ControllerUI.BucleProducto(carta2Flan, 20, "Flan", "Leche", "Leche", 140);
            ControllerUI.BucleProducto(carta3Helado, 30, "Helado", "Fruta", "Leche", 150);
            ControllerUI.BucleProducto(carta4Jugo, 40, "Jugo", "Fruta", "Fruta", 170);
            ControllerUI.BucleProducto(carta5Pie, 50, "Pie", "Harina", "Fruta", 200);
            ControllerUI.BucleProducto(carta6Pionono, 60, "Pionono", "Huevo", "Leche", 230);
            ControllerUI.BucleProducto(carta7Torta, 70, "Torta", "Harina", "Huevo", 240);

        }


        void ReconocerJugador()
        {
            for (int i = 0; i < ValidacionCampos.cantidadJugadores; i++)
            {
                if (ValidacionCampos.listaJugadores[i].Turno)
                {
                    indiceJugador = i;
                    break;
                }
            }
        }

        //public static void DarItemProductoVista(string op)
        //{
        //    CambiarItemProducto(op);
        //}
        //Esta estoy dudando si ponerla en controlador o no 
        public void CambiarItemProducto(string opcion)
        {
            ReconocerJugador();
            int cantidadCartas = ValidacionCampos.listaJugadores[indiceJugador].Cartas.Count;
            if (opcion == "next")
            {
                indexItem++;
                if (indexItem == cantidadCartas + 1)
                {
                    indexItem = 1;
                }
            }
            else if (opcion == "back")
            {
                indexItem--;
                if (indexItem == 0)
                {
                    indexItem = cantidadCartas;
                }
            }
            else if (opcion == "")
            {
                indexItem = 1;
            }
            int indexMateriaUno = 0, indexMateriaDos = 0;
            txtTitulo.text = ValidacionCampos.listaJugadores[indiceJugador].Cartas[indexItem - 1].Nombre;
            txtPrecio.text = ValidacionCampos.listaJugadores[indiceJugador].Cartas[indexItem - 1].Monto.ToString();
            //----------------------------------------------------------------------------------------------------
            if (ValidacionCampos.listaJugadores[indiceJugador].Cartas[indexItem - 1].Materia_uno == "Leche")
            {
                indexMateriaUno = 0;
            }
            else if (ValidacionCampos.listaJugadores[indiceJugador].Cartas[indexItem - 1].Materia_uno == "Huevo")
            {
                indexMateriaUno = 1;
            }
            else if (ValidacionCampos.listaJugadores[indiceJugador].Cartas[indexItem - 1].Materia_uno == "Fruta")
            {
                indexMateriaUno = 2;
            }
            else if (ValidacionCampos.listaJugadores[indiceJugador].Cartas[indexItem - 1].Materia_uno == "Harina")
            {
                indexMateriaUno = 3;
            }
            //----------------------------------------------------------------------------------------------------
            if (ValidacionCampos.listaJugadores[indiceJugador].Cartas[indexItem - 1].Materia_dos == "Leche")
            {
                indexMateriaDos = 0;
            }
            else if (ValidacionCampos.listaJugadores[indiceJugador].Cartas[indexItem - 1].Materia_dos == "Huevo")
            {
                indexMateriaDos = 1;
            }
            else if (ValidacionCampos.listaJugadores[indiceJugador].Cartas[indexItem - 1].Materia_dos == "Fruta")
            {
                indexMateriaDos = 2;
            }
            else if (ValidacionCampos.listaJugadores[indiceJugador].Cartas[indexItem - 1].Materia_dos == "Harina")
            {
                indexMateriaDos = 3;
            }

            iconoMateria[0].sprite = imagenesMateria[indexMateriaUno];
            iconoMateria[1].sprite = imagenesMateria[indexMateriaDos];
            GetComponent<OnClick>().SonidoBoton();

        }
        public static IEnumerator MostrarAviso(GameObject modal, Text goAviso, string txtAviso)
        {
            modal.SetActive(true);
            goAviso.text = txtAviso;
            yield return new WaitForSeconds(3);
            modal.SetActive(false);
        }

        public void Vender(Text txtMateria)
        {
            ReconocerJugador();
            ReconocerCarta(txtMateria);
            GetComponent<ClickAcciones>().CerrarModal();


        }

        private void ReconocerCarta(Text txtMateria)
        {
            //int posicionCarta;
            for (int i = 0; i < ValidacionCampos.listaJugadores[indiceJugador].Cartas.Count; i++)
            {
                if (ValidacionCampos.listaJugadores[indiceJugador].Cartas[i].Nombre == txtMateria.text)
                {
                    if (ControllerUI.VerificarVenta(i))
                    {
                        float monto = ValidacionCampos.listaJugadores[indiceJugador].Cartas[i].Monto;
                        ValidacionCampos.listaJugadores[indiceJugador].DineroActual = ValidacionCampos.listaJugadores[indiceJugador].DineroActual + monto;
                        //posicionCarta = (ValidacionCampos.listaJugadores[indiceJugador].Cartas[i].IdentificadorMateria/10)-1;
                        ValidacionCampos.listaJugadores[indiceJugador].Cartas.RemoveAt(i);
                        GetComponent<OnClick>().SonidoDinero();
                        StartCoroutine(MostrarAviso(modalAviso, txtAviso, txtMateria.text + " vendido"));

                        if (ValidacionCampos.listaJugadores[indiceJugador].Cartas.Count == 0)
                        {
                            //desactivar la opcion venta
                            btnVender.SetActive(false);
                        }

                    }
                    else
                    {
                        GetComponent<OnClick>().OtroSonido(avisoAudioClip);
                        StartCoroutine(MostrarAviso(modalAviso, txtAviso, "No tienes las suficientes materias para este producto"));
                    }

                }
            }
        }

        public void AvisoSonido()
        {
            GetComponent<OnClick>().OtroSonido(avisoAudioClip);
        }
        public void CastigoSonido()
        {
            GetComponent<OnClick>().OtroSonido(castigoAudioClip);
        }

    }
}
