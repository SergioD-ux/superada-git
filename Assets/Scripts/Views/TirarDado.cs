﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Assets.Scripts.Models;



public class TirarDado : MonoBehaviour
{
    // Start is called before the first frame update
    private Animator animatorplayer;

    public List<Sprite> imgListDados;
    Image imgDados;
    public static bool estadoTirarClick = true;
    public List<Transform> posCasillero1;
    public List<Transform> posCasillero2;
    public List<Transform> posCasillero3;
    public List<Transform> posCasillero4;

    public List<Transform> jugadorFicha = new List<Transform>();
    int cantJugadores;

    public GameObject btnComprar, btnVender, modalTurno,modalAviso;
    public Text txtAviso,txtPrecioMateria,txtNombre;

    bool paseSeguir=false;//Para cuabdo dan una carta producto
    //List<Button> buttons;

    //Audio
    AudioSource dadoAudioSource;
    public AudioClip dadoAudioClip,ganardineroClip;
    
    //List<Jugador> listaJugadores=new List<Jugador>();
    //Jugador jugador1;
    //Jugador jugador2;
    //int espacioActual;

    public void ReproducirAnimacion(string state = null)
    {
        if (state != null)
        {
            animatorplayer.Play(state);
        }
    }

    private void SonidoDinero()
    {
        dadoAudioSource.clip = ganardineroClip;
        dadoAudioSource.Play();
    }
    void Start()
    {
        animatorplayer = GetComponent<Animator>();
        dadoAudioSource = GetComponent<AudioSource>();
        //Debug.Log(ValidacionCampos.listaJugadores[0].PosicionActual);

        cantJugadores = ValidacionCampos.listaJugadores.Count;
        imgDados = GetComponent<Image>();
        imgDados.sprite = imgListDados[0];

        ValidacionCampos.listaJugadores[0].Turno = true;

        for (int i=0; i<ValidacionCampos.listaJugadores.Count;i++)
        {
            if (ValidacionCampos.listaJugadores[i].Activado )
            {
                ValidacionCampos.listaJugadores[i].Posicion = jugadorFicha[i];
                jugadorFicha[i].gameObject.SetActive(true);
            }
            else
            {
                jugadorFicha[i].gameObject.SetActive(false);
            }
            
        }

        StartCoroutine(MostrarModalTruno(modalTurno, txtNombre, "Turno de " + ValidacionCampos.listaJugadores[0].Nombre));

    }

    void SonidoDado()
    {
        dadoAudioSource.clip = dadoAudioClip;
        dadoAudioSource.Play();
    }

    IEnumerator MostrarNumeroDado(int numero)
    {
        string nombreBase = "MovimientoDado";
        string numeroDado = (numero + 1).ToString();
        Debug.Log("NumeroAnimacion:" + numeroDado);
        string nombreAnimacion = nombreBase + numeroDado;
        Debug.Log("NombreAnimacion:" + nombreAnimacion);
        ReproducirAnimacion(nombreAnimacion);
        SonidoDado();
        yield return new WaitForSeconds(0.5f);
        imgDados.sprite = imgListDados[numero];
        estadoTirarClick = false;
    }

    public void Tirar()
    {
        if (estadoTirarClick)
        {
            int numeroRandom = Random.Range(0, 6);

            //link de scripting de dado: https://www.youtube.com/watch?v=NKdH5TpZtzM
            //string nombreBase = "MovimientoDado";
            //string numeroDado = (numeroRandom + 1).ToString();
            //Debug.Log("NumeroAnimacion:" + numeroDado);
            //string nombreAnimacion = nombreBase + numeroDado;
            //Debug.Log("NombreAnimacion:"+nombreAnimacion);
            //ReproducirAnimacion(nombreAnimacion);
            //SonidoDado();

            //imgDados.sprite = imgListDados[numeroRandom];

            //StartCoroutine(MostrarNumeroDado(numeroRandom));
            SonidoDado();
            imgDados.sprite = imgListDados[numeroRandom];
            MoverJugador(numeroRandom+1,cantJugadores);
            estadoTirarClick = false;
        }
    }

    public void MoverJugador(int espacios,int cantJugadores)
    {
        
        for (int i = 0; i < cantJugadores; i++)
        {
            if (ValidacionCampos.listaJugadores[i].Turno)
            {
                ValidacionCampos.listaJugadores[i].PosicionActual = ValidacionCampos.listaJugadores[i].PosicionActual+ espacios;
                Debug.Log(ValidacionCampos.listaJugadores[i].Turno);
                if (ValidacionCampos.listaJugadores[i].PosicionActual > 31)
                {
                    ValidacionCampos.listaJugadores[i].PosicionActual = ValidacionCampos.listaJugadores[i].PosicionActual -  32;
                }
                switch (ValidacionCampos.listaJugadores[i].NumeroJugador)
                {
                    case 1:
                        ValidacionCampos.listaJugadores[i].Posicion.position = new Vector2(posCasillero1[ValidacionCampos.listaJugadores[i].PosicionActual].position.x, posCasillero1[ValidacionCampos.listaJugadores[i].PosicionActual].position.y);
                        break;
                    case 2:
                        ValidacionCampos.listaJugadores[i].Posicion.position = new Vector2(posCasillero2[ValidacionCampos.listaJugadores[i].PosicionActual].position.x, posCasillero2[ValidacionCampos.listaJugadores[i].PosicionActual].position.y);
                        break;
                    case 3:
                        ValidacionCampos.listaJugadores[i].Posicion.position = new Vector2(posCasillero3[ValidacionCampos.listaJugadores[i].PosicionActual].position.x, posCasillero3[ValidacionCampos.listaJugadores[i].PosicionActual].position.y);
                        break;
                    case 4:
                        ValidacionCampos.listaJugadores[i].Posicion.position = new Vector2(posCasillero4[ValidacionCampos.listaJugadores[i].PosicionActual].position.x, posCasillero4[ValidacionCampos.listaJugadores[i].PosicionActual].position.y);
                        break;
                    default:
                        break;
                }

                AccionCasillero(i);
                //CambiarTurno(i, cantJugadores);
                //ActivarBotonesAccion(ValidacionCampos.listaJugadores[i].PosicionActual);
                //AccionCasillero(i);
                break;
            }
        }
    }

    IEnumerator MostrarModalTruno(GameObject modal,Text txtNombre,string nombre)
    {
        txtNombre.text = nombre;
        modal.SetActive(true);
        yield return new WaitForSeconds(1);
        modal.SetActive(false);
    }

    public void Seguir()
    {
        if (ClickAcciones.ExisteDeuda)
        {
            StartCoroutine(MostrarAviso(modalAviso,txtAviso, "Tienes que pagar " + ClickAcciones.MontoDeuda + ". Ada recomienda un préstamo"));
            GetComponent<ActualizarUI>().AvisoSonido();
        }
        else
        {

            int numeroJugador = 0;
            Debug.Log(ValidacionCampos.listaJugadores.Count);
            for (int i = 0; i < ValidacionCampos.listaJugadores.Count; i++)
            {
                if (ValidacionCampos.listaJugadores[i].Turno)
                {
                    numeroJugador = i;
                    break;
                }
            }
            if (PuedeVender(numeroJugador) && ClickAcciones.casillero=="Cliente" && !paseSeguir)
            {
                StartCoroutine(MostrarAviso(modalAviso, txtAviso, "Tienes un producto por vender"));
                GetComponent<ActualizarUI>().AvisoSonido();
            }
            else
            {
                int numeroSiguiente = numeroJugador + 1; ;
                if (numeroSiguiente == cantJugadores)
                {
                    numeroSiguiente = 0;
                }
                ValidacionCampos.listaJugadores[numeroJugador].Turno = false;
                ValidacionCampos.listaJugadores[numeroSiguiente].Turno = true;
                GetComponent<OnClick>().SonidoBoton();
                //AccionCasillero(numeroSiguiente);
                StartCoroutine(MostrarModalTruno(modalTurno, txtNombre,"Turno de "+ ValidacionCampos.listaJugadores[numeroSiguiente].Nombre));
                estadoTirarClick = true;
                paseSeguir = false;
            }
        }
    }

    void ActivarBotonesAccion(GameObject objActivar,GameObject objDesactivar,bool desactivartodo)
    {
        if (desactivartodo)
        {
            objActivar.SetActive(false);
            objDesactivar.SetActive(false);
        }
        else
        {
            objActivar.SetActive(true);
            objDesactivar.SetActive(false);
        }
        
    }
    private CartaProducto DarCarta(int indiceCarta)
    {
        CartaProducto carta = new CartaProducto();
        carta.IdentificadorMateria =ActualizarUI.listaCartaProductos[indiceCarta].IdentificadorMateria;
        carta.IdentificadorIndice = ActualizarUI.listaCartaProductos[indiceCarta].IdentificadorIndice;
        carta.Nombre = ActualizarUI.listaCartaProductos[indiceCarta].Nombre;
        carta.Materia_uno = ActualizarUI.listaCartaProductos[indiceCarta].Materia_uno;
        carta.Materia_dos = ActualizarUI.listaCartaProductos[indiceCarta].Materia_dos;
        carta.Monto = ActualizarUI.listaCartaProductos[indiceCarta].Monto;
        carta.Ocupado = true;
        carta.Disponible = true;
        return carta;
    }
    public void AsignarOtraCarta(int indiceJugador)
    {
        int numeroRandom = Random.Range(0, 35);
        ValidacionCampos.listaJugadores[indiceJugador].Cartas.Add(DarCarta(numeroRandom));

    }

    public void AccionCasillero(int indiceJugador)
    {
        switch (ValidacionCampos.listaJugadores[indiceJugador].PosicionActual)
        {
            case 1:
                //Casillero Leche1
                ActivarBotonesAccion(btnComprar, btnVender,false);
                ClickAcciones.casillero = "MateriaPrima1";
                txtPrecioMateria.text = "50 c/u";
                break;                                    
            case 2:               
                //Casillero Leche2
                ActivarBotonesAccion(btnComprar, btnVender,false);
                ClickAcciones.casillero = "MateriaPrima1";
                txtPrecioMateria.text = "50 c/u";
                break;                                    
            case 3:               
                //Casillero Leche3
                ActivarBotonesAccion(btnComprar, btnVender,false);
                ClickAcciones.casillero = "MateriaPrima1";
                txtPrecioMateria.text = "50 c/u";
                break;                                     
            case 4:               
                //Casillero MPTodos
                ActivarBotonesAccion(btnComprar, btnVender,false);
                ClickAcciones.casillero = "MateriaPrimaTodos";
                txtPrecioMateria.text = "60 c/u";
                break;                                     
            case 5:                
                //Casillero MPTodos
                ActivarBotonesAccion(btnComprar, btnVender,false);
                ClickAcciones.casillero = "MateriaPrimaTodos";
                txtPrecioMateria.text = "60 c/u";
                break;                                     
            case 6:                
                //Casillero MPTodos
                ActivarBotonesAccion(btnComprar, btnVender,false);
                ClickAcciones.casillero = "MateriaPrimaTodos";
                txtPrecioMateria.text = "60 c/u";
                break;                                     
            case 7:                
                //Casillero Cliente
                ActivarBotonesAccion(btnVender, btnComprar,false);
                ClickAcciones.casillero = "Cliente";
                AccionCasilleroCliente(indiceJugador);
                break;                                     
            case 8:                
                //Casillero Cliente
                ActivarBotonesAccion(btnVender, btnComprar,false);
                ClickAcciones.casillero = "Cliente";
                AccionCasilleroCliente(indiceJugador);
                break;                                     
            case 9:
                //Casillero Cliente
                ActivarBotonesAccion(btnVender, btnComprar,false);
                ClickAcciones.casillero = "Cliente";
                AccionCasilleroCliente(indiceJugador);
                break;                                     
            case 10:
                //Casillero Cliente
                ActivarBotonesAccion(btnVender, btnComprar,false);
                ClickAcciones.casillero = "Cliente";
                AccionCasilleroCliente(indiceJugador);
                break;                                     
            case 11:
                //Casillero Huevo1
                ActivarBotonesAccion(btnComprar, btnVender,false);
                ClickAcciones.casillero = "MateriaPrima2";
                txtPrecioMateria.text = "50 c/u";
                break;
            case 12:
                //Casillero Huevo2
                ActivarBotonesAccion(btnComprar, btnVender, false);
                ClickAcciones.casillero = "MateriaPrima2";
                txtPrecioMateria.text = "50 c/u";
                break;
            case 13:
                //Casillero Castigo1
                ActivarBotonesAccion(btnVender, btnComprar, true);
                ClickAcciones.casillero = "Castigo20";
                GetComponent<ActualizarUI>().CastigoSonido();
                break;
            case 14:
                //Casillero Recompensa1
                ActivarBotonesAccion(btnVender, btnComprar, true);
                ClickAcciones.casillero = "Recompensa30";
                SonidoDinero();
                break;
            case 15:
                //Casillero Castigo2
                ActivarBotonesAccion(btnVender, btnComprar, true);
                ClickAcciones.casillero = "Castigo50";
                GetComponent<ActualizarUI>().CastigoSonido();
                break;
            case 16:
                //Casillero Cliente
                ActivarBotonesAccion(btnVender, btnComprar, false);
                ClickAcciones.casillero = "Cliente";
                AccionCasilleroCliente(indiceJugador);
                break;
            case 17:
                //Casillero Cliente
                ActivarBotonesAccion(btnVender, btnComprar, false);
                ClickAcciones.casillero = "Cliente";
                AccionCasilleroCliente(indiceJugador);
                break;
            case 18:
                //Casillero Fruta1
                ActivarBotonesAccion(btnComprar, btnVender, false);
                ClickAcciones.casillero = "MateriaPrima3";
                txtPrecioMateria.text = "50 c/u";
                break;
            case 19:
                //Casillero Fruta1
                ActivarBotonesAccion(btnComprar, btnVender, false);
                ClickAcciones.casillero = "MateriaPrima3";
                txtPrecioMateria.text = "50 c/u";
                break;
            case 20:
                //Casillero Fruta1
                ActivarBotonesAccion(btnComprar, btnVender, false);
                ClickAcciones.casillero = "MateriaPrima3";
                txtPrecioMateria.text = "50 c/u";
                break;
            case 21:
                //Casillero Recompensa2
                ActivarBotonesAccion(btnVender, btnComprar, true);
                ClickAcciones.casillero = "Recompensa50";
                SonidoDinero();
                break;
            case 22:
                //Casillero Recompensa3
                ActivarBotonesAccion(btnVender, btnComprar, true);
                ClickAcciones.casillero = "Recompensa20";
                SonidoDinero();
                break;
            case 23:
                //Casillero Cliente
                ActivarBotonesAccion(btnVender, btnComprar, false);
                ClickAcciones.casillero = "Cliente";
                AccionCasilleroCliente(indiceJugador);
                break;
            case 24:
                //Casillero Cliente
                ActivarBotonesAccion(btnVender, btnComprar, false);
                ClickAcciones.casillero = "Cliente";
                AccionCasilleroCliente(indiceJugador);
                break;
            case 25:
                //Casillero Harina1
                ActivarBotonesAccion(btnComprar, btnVender, false);
                ClickAcciones.casillero = "MateriaPrima4";
                txtPrecioMateria.text = "50 c/u";
                break;
            case 26:
                //Casillero Harina2
                ActivarBotonesAccion(btnComprar, btnVender, false);
                ClickAcciones.casillero = "MateriaPrima4";
                txtPrecioMateria.text = "50 c/u";
                break;
            case 27:
                //Casillero Cliente
                ActivarBotonesAccion(btnVender, btnComprar, false);
                ClickAcciones.casillero = "Cliente";
                AccionCasilleroCliente(indiceJugador);
                break;
            case 28:
                //Casillero Cliente
                ActivarBotonesAccion(btnVender, btnComprar, false);
                ClickAcciones.casillero = "Cliente";
                AccionCasilleroCliente(indiceJugador);
                break;
            case 29:
                //Casillero Castigo3
                ActivarBotonesAccion(btnVender, btnComprar, true);
                ClickAcciones.casillero = "Castigo20";
                GetComponent<ActualizarUI>().CastigoSonido();
                break;
            case 30:
                //Casillero Castigo4
                ActivarBotonesAccion(btnVender, btnComprar, true);
                ClickAcciones.casillero = "Castigo50";
                GetComponent<ActualizarUI>().CastigoSonido();
                break;
            case 31:
                ActivarBotonesAccion(btnVender, btnComprar, true);
                ClickAcciones.casillero = "Castigo100";
                GetComponent<ActualizarUI>().CastigoSonido();
                //Casillero Castigo5
                break;
            default:
                //Sera posicion 0 o mejor dicho inicio
                ActivarBotonesAccion(btnVender, btnComprar, true);
                ClickAcciones.casillero = "Inicio";
                break;
        }
    }

    void AccionCasilleroCliente(int indiceJugador)
    {
        if (PuedeVender(indiceJugador))
        {
            StartCoroutine(MostrarAviso(modalAviso, txtAviso,"Puedes vender un producto"));
            GetComponent<OnClick>().SonidoBoton();
        }
        else{
            btnVender.gameObject.SetActive(false);
            if (ValidacionCampos.listaJugadores[indiceJugador].Cartas.Count < 5)
            {
                AsignarOtraCarta(indiceJugador);
                if (ValidacionCampos.listaJugadores[indiceJugador].Cartas.Count==1 || PuedeVender(indiceJugador))
                {
                    StartCoroutine(MostrarAviso(modalAviso, txtAviso, "Se te dio otra carta. Para venderla espera al otro casillero de Cliente"));
                    
                    paseSeguir = true;
                    GetComponent<ActualizarUI>().AvisoSonido();
                }
                else
                {
                    StartCoroutine(MostrarAviso(modalAviso, txtAviso, "No tienes para vender un producto. Se te dará otro"));
                    GetComponent<ActualizarUI>().AvisoSonido();
                }
                
            }
            else
            {
                StartCoroutine(MostrarAviso(modalAviso, txtAviso, "Ya tienes 5 productos sin vender.Ada recomienda satisfacer a tus clientes"));
                GetComponent<ActualizarUI>().AvisoSonido();
            }
            
        }
    }
    
    public static IEnumerator MostrarAviso(GameObject modal,Text goAviso, string txtAviso)
    {
        modal.SetActive(true);
        goAviso.text = txtAviso;
        yield return new WaitForSeconds(3);
        modal.SetActive(false);
    }

    public static bool PuedeVender(int indiceJugador)
    {
        
        bool respuesta= false;
        int cantidadCartas = ValidacionCampos.listaJugadores[indiceJugador].Cartas.Count;
        for (int i=0;i<cantidadCartas;i++)
        {
            string materia1= ValidacionCampos.listaJugadores[indiceJugador].Cartas[i].Materia_uno;
            string materia2= ValidacionCampos.listaJugadores[indiceJugador].Cartas[i].Materia_dos;
            int cantidadLeche= ValidacionCampos.listaJugadores[indiceJugador].CantidadMaterial1;
            int cantidadHuevo= ValidacionCampos.listaJugadores[indiceJugador].CantidadMaterial2;
            int cantidadFruta = ValidacionCampos.listaJugadores[indiceJugador].CantidadMaterial3;
            int cantidadHarina = ValidacionCampos.listaJugadores[indiceJugador].CantidadMaterial4;

            if (materia1 == "Leche" && cantidadLeche > 0)
            {
                if (materia2 == "Leche" && cantidadLeche > 1)
                {

                    respuesta = true;
                    break;
                }
                else if (materia2 == "Huevo" && cantidadHuevo > 0)
                {
                    respuesta = true;
                    break;
                }
                else if (materia2 == "Fruta" && cantidadFruta > 0)
                {
                    respuesta = true;
                    break;
                }
                else if (materia2 == "Harina" && cantidadHarina > 0)
                {
                    respuesta = true;
                    break;
                }
            }
            //-----------------------------------------------------
            else if (materia1=="Huevo" && cantidadHuevo>0)
            {
                if (materia2 == "Leche" && cantidadLeche > 0)
                {

                    respuesta = true;
                    break;
                }
                else if (materia2 == "Huevo" && cantidadHuevo > 1)
                {
                    respuesta = true;
                    break;
                }
                else if (materia2 == "Fruta" && cantidadFruta > 0)
                {
                    respuesta = true;
                    break;
                }
                else if (materia2 == "Harina" && cantidadHarina > 0)
                {
                    respuesta = true;
                    break;
                }
            }
            //-------------------------------------------
            else if (materia1 == "Fruta"  && cantidadFruta > 0)
            {
                if (materia2 == "Leche" && cantidadLeche > 0)
                {

                    respuesta = true;
                    break;
                }
                else if (materia2 == "Huevo" && cantidadHuevo > 0)
                {
                    respuesta = true;
                    break;
                }
                else if (materia2 == "Fruta" && cantidadFruta > 1)
                {
                    respuesta = true;
                    break;
                }
                else if (materia2 == "Harina" && cantidadHarina > 0)
                {
                    respuesta = true;
                    break;
                }
            }
            //-----------------------------------------------------
            else if (materia1 == "Harina"  && cantidadHarina > 0)
            {
                if (materia2 == "Leche" && cantidadLeche > 0)
                {

                    respuesta = true;
                    break;
                }
                else if (materia2 == "Huevo" && cantidadHuevo > 0)
                {
                    respuesta = true;
                    break;
                }
                else if (materia2 == "Fruta" && cantidadFruta > 0)
                {
                    respuesta = true;
                    break;
                }
                else if (materia2 == "Harina" && cantidadHarina > 1)
                {
                    respuesta = true;
                    break;
                }
            }
        }
        return respuesta;
    }

    bool ValidarDinero(int indice)
    {
        bool respuesta = false;
        if (ValidacionCampos.listaJugadores[indice].DineroActual>=50)
        {
            respuesta = true;
        }
        return respuesta;
    }

    void CambiarTurno(int numeroJugador, int cantJugadores)
    {
        
        int numeroSiguiente = numeroJugador + 1; ;
        if (numeroSiguiente == cantJugadores)
        {
            numeroSiguiente = 0;
        }
        ValidacionCampos.listaJugadores[numeroJugador].Turno = false;
        ValidacionCampos.listaJugadores[numeroSiguiente].Turno = true;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
