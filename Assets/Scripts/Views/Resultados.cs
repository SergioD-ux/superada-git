﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Assets.Scripts.Models;
using UnityEngine.Networking;

public class Resultados : MonoBehaviour
{
    public List<Text> txtNombre,txtIngreso,txtIngresoExtra, txtBanco, txtSocio, txtUtilidad;
    public List<Image> imgIconos;

    private void Start()
    {
        DarResultados();
    }

    private void EnviarEstadistica()
    {
        string nro_jugadores="";
        string nombres="";
        string ingresos = "";
        string utilidades = "";
        string cant_prestBancario = "";
        string cant_prestSocio = "";
        string dinero_prestBancario = "";
        string dinero_prestSocio = "";
        string ordenPuesto = "";
        string cantFinalLeche = "";
        string cantFinalHuevo = "";
        string cantFinalFruta = "";
        string cantFinalHarina = "";
        string cantProductosSobrantes = "";

        nro_jugadores = ValidacionCampos.listaJugadores.Count.ToString();
        for (int i = 0; i < ValidacionCampos.listaJugadores.Count; i++)
        {
            nombres += ValidacionCampos.listaJugadores[i].Nombre+" / ";
            ingresos += ValidacionCampos.listaJugadores[i].DineroActual.ToString() + " / ";
            utilidades += ValidacionCampos.listaJugadores[i].DineroGanado.ToString() + " / ";
            cant_prestBancario += ValidacionCampos.listaJugadores[i].NumeroTarjetasPrestamoBanco.ToString() + " / ";
            cant_prestSocio += ValidacionCampos.listaJugadores[i].NumeroTarjetasPrestamoSocio.ToString() + " / ";
            dinero_prestBancario += ValidacionCampos.listaJugadores[i].PrestamoBancario.ToString() + " / ";
            dinero_prestSocio += ValidacionCampos.listaJugadores[i].PrestamoSocio.ToString() + " / ";
            ordenPuesto += ValidacionCampos.listaJugadores[i].OrdenPuesto.ToString() + " / ";
            cantFinalLeche += ValidacionCampos.listaJugadores[i].CantidadMaterial1.ToString() + " / ";
            cantFinalHuevo += ValidacionCampos.listaJugadores[i].CantidadMaterial2.ToString() + " / ";
            cantFinalFruta += ValidacionCampos.listaJugadores[i].CantidadMaterial3.ToString() + " / ";
            cantFinalHarina += ValidacionCampos.listaJugadores[i].CantidadMaterial4.ToString() + " / ";
            cantProductosSobrantes += ValidacionCampos.listaJugadores[i].Cartas.Count.ToString() + " / ";
        }

        StartCoroutine(PostFormEstadisticas(nro_jugadores,nombres,ingresos,utilidades,cant_prestBancario,cant_prestSocio,dinero_prestBancario,dinero_prestSocio,ordenPuesto,cantFinalLeche,cantFinalHuevo,cantFinalFruta,cantFinalHarina,cantProductosSobrantes));

    }

    IEnumerator PostFormEstadisticas(string nro_jugadores, string nombres, string ingresos, string utilidades, string cant_prestBancario,string cant_prestSocio,string dinero_prestBancario,string dinero_prestSocio,string ordenPuesto,string cantFinalLeche,string cantFinalHuevo,string cantFinalFruta,string cantFinalHarina,string cantProductosSobrantes)
    {
        WWWForm form = new WWWForm();
        form.AddField("entry.670188985", nro_jugadores);    
        form.AddField("entry.1802527707", nombres);
        form.AddField("entry.1197966829", ingresos);
        form.AddField("entry.1731884330", utilidades);
        form.AddField("entry.1706214660", cant_prestBancario);
        form.AddField("entry.1702861716", cant_prestSocio);
        form.AddField("entry.760976599", dinero_prestBancario);
        form.AddField("entry.2059296278", dinero_prestSocio);
        form.AddField("entry.1669550311", ordenPuesto);
        form.AddField("entry.1858388095", cantFinalLeche);
        form.AddField("entry.491413726", cantFinalHuevo);
        form.AddField("entry.1538053658", cantFinalFruta);
        form.AddField("entry.1564263027", cantFinalHarina);
        form.AddField("entry.238299121", cantProductosSobrantes);
        byte[] rawData = form.data;
        UnityWebRequest www = UnityWebRequest.Post("https://docs.google.com/forms/u/1/d/e/1FAIpQLSfgjKv0wJ-49ZDmqR_tjSS_-vhtRJ23sFfG0KZmr_Om14hxvA/formResponse", form);
        yield return www.SendWebRequest();
    }
    public void DarResultados()
    {
        OperarData();
        OrdenarPuestos();

        for (int i = 0; i < ValidacionCampos.listaJugadores.Count; i++)
        {
            txtNombre[i].text = ValidacionCampos.listaJugadores[i].Nombre;
            txtNombre[i].gameObject.SetActive(true);

            txtIngreso[i].text = ValidacionCampos.listaJugadores[i].DineroActual.ToString();
            txtIngreso[i].gameObject.SetActive(true);

            float cantMateria1 = ValidacionCampos.listaJugadores[i].CantidadMaterial1;
            float cantMateria2 = ValidacionCampos.listaJugadores[i].CantidadMaterial2;
            float cantMateria3 = ValidacionCampos.listaJugadores[i].CantidadMaterial3;
            float cantMateria4 = ValidacionCampos.listaJugadores[i].CantidadMaterial4;

            txtIngresoExtra[i].text = ((cantMateria1+cantMateria2+cantMateria3+cantMateria4)*30).ToString();
            txtIngresoExtra[i].gameObject.SetActive(true);

            txtBanco[i].text = ValidacionCampos.listaJugadores[i].PrestamoBancarioDevolucionTotal.ToString();
            txtBanco[i].gameObject.SetActive(true);

            txtSocio[i].text = ValidacionCampos.listaJugadores[i].PrestamoSocioDevolucionTotal.ToString();
            txtSocio[i].gameObject.SetActive(true);

            txtUtilidad[i].text = ValidacionCampos.listaJugadores[i].DineroGanado.ToString();
            txtUtilidad[i].gameObject.SetActive(true);

            imgIconos[i].gameObject.SetActive(true);

        }
        EnviarEstadistica();

    }

    private void OperarData()
    {
        for (int i =0;i<ValidacionCampos.listaJugadores.Count;i++)
        {
            float dineroActual= ValidacionCampos.listaJugadores[i].DineroActual;

            float cantMateria1 = ValidacionCampos.listaJugadores[i].CantidadMaterial1;
            float cantMateria2 = ValidacionCampos.listaJugadores[i].CantidadMaterial2;
            float cantMateria3 = ValidacionCampos.listaJugadores[i].CantidadMaterial3;
            float cantMateria4= ValidacionCampos.listaJugadores[i].CantidadMaterial4;

            float ingresoExtraMaterias = (cantMateria1+cantMateria2+cantMateria3+cantMateria4) * 30;

            dineroActual = dineroActual + ingresoExtraMaterias;

            float prestBancoBruto = ValidacionCampos.listaJugadores[i].PrestamoBancario;
            float prestBancoTotal;
            if (prestBancoBruto==0)
            {
                prestBancoTotal = 0;
            }
            else
            {
                float prestBancoComision = prestBancoBruto * 0.15f;
                prestBancoTotal = prestBancoBruto + prestBancoComision;
            }
            ValidacionCampos.listaJugadores[i].PrestamoBancarioDevolucionTotal = prestBancoTotal;

            

            float prestSocioBruto = ValidacionCampos.listaJugadores[i].PrestamoSocio;
            float dineroResto;
            float prestSocioTotal;
            if (prestSocioBruto==0)
            {
                prestSocioTotal = 0;
            }
            else
            {
                dineroResto = dineroActual - prestSocioBruto;
                float prestSocioComision = dineroResto * 0.15f;
                prestSocioTotal = prestSocioBruto + prestSocioComision;
            }

            ValidacionCampos.listaJugadores[i].PrestamoSocioDevolucionTotal = prestSocioTotal;


            float devolucionTotal = prestBancoTotal + prestSocioTotal;

            float utlidad = dineroActual - devolucionTotal;

            ValidacionCampos.listaJugadores[i].DineroGanado=utlidad;


        }
    }

    private void OrdenarPuestos()
    {
        
        for (int i = 1; i < ValidacionCampos.listaJugadores.Count; i++)
        {

            for (int j = 0; j < ValidacionCampos.listaJugadores.Count - i; j++)
            {
                if (ValidacionCampos.listaJugadores[j].DineroGanado < ValidacionCampos.listaJugadores[j+1].DineroGanado)
                {
                    Jugador jugadroTemp = new Jugador();
                    jugadroTemp = ValidacionCampos.listaJugadores[j];
                    ValidacionCampos.listaJugadores[j] = ValidacionCampos.listaJugadores[j+1];
                    ValidacionCampos.listaJugadores[j+1] = jugadroTemp;
                }
            }

        }

    }



}