﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.Networking;

public class Valoracion : MonoBehaviour
{
    // Start is called before the first frame update

    public Text txtNombre;
    public Text txtEdad;
    public Text txtCiudad;
    public Text txtValoracion;
    public Text txtMensaje;
    public Text txtAviso;
    void Start()
    {
        
    }

    
    public bool ValidarCampos()
    {
        bool ValidacionCorrectamente = false;
        if (txtNombre.text != "" && txtEdad.text != "" && txtCiudad.text != "" && txtValoracion.text != "" )
        {
            if (int.Parse(txtValoracion.text)>0 && int.Parse(txtValoracion.text) < 6)
            {
                ValidacionCorrectamente = true;
            }
            else
            {
                StartCoroutine(MostrarAviso(txtAviso, "La valoracion es del 1 al 5"));
            }
            
        }
        else{
            StartCoroutine(MostrarAviso(txtAviso, "Debes completar todos los campos"));

        }
        return ValidacionCorrectamente;
    }

    IEnumerator PostForm(string nombre,string edad,string ciudad,string valoracion,string mensaje)
    {
        WWWForm form = new WWWForm();
        form.AddField("entry.751928415", nombre);
        form.AddField("entry.2140489126", edad);
        form.AddField("entry.627090367", ciudad);
        form.AddField("entry.821246508", valoracion);
        form.AddField("entry.479136175", mensaje);
        byte[] rawData = form.data;
        UnityWebRequest www = UnityWebRequest.Post("https://docs.google.com/forms/u/1/d/e/1FAIpQLSf44N66K7MVMq9KZ3uFC8CDJTriCrs2iMGEqBe-ePDg2JbO4w/formResponse", form);
        yield return www.SendWebRequest();
    }

    public void Enviar()
    {
        //string urlBase = "https://docs.google.com/forms/u/1/d/e/1FAIpQLSf44N66K7MVMq9KZ3uFC8CDJTriCrs2iMGEqBe-ePDg2JbO4w/formResponse";
        string nombre = txtNombre.text.ToString();
        string edad = txtEdad.text.ToString();
        string ciudad = txtCiudad.text.ToString();
        string valoracion = txtValoracion.text.ToString();
        string mensaje = txtMensaje.text.ToString();
        //string urlCompleto = urlBase + "?entry.751928415=" + nombre + "&entry.2140489126=" + edad + "&entry.627090367=" + ciudad + "&entry.821246508=" + valoracion + "&entry.479136175=" + mensaje;
        if (ValidarCampos())
        {

            //Application.OpenURL(urlCompleto);
            StartCoroutine(PostForm(nombre,edad,ciudad,valoracion,mensaje));
            SceneManager.LoadScene("Menu");

        }

    }

    IEnumerator MostrarAviso(Text goAviso, string txtAviso)
    {
        goAviso.gameObject.SetActive(true);
        goAviso.text = txtAviso;
        yield return new WaitForSeconds(3);
        goAviso.gameObject.SetActive(false);
    }
}
