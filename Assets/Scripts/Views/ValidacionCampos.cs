﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Assets.Scripts.Models;
using UnityEngine.SceneManagement;

public class ValidacionCampos : MonoBehaviour
{
    //public List<Text> CamposTxt;
    public Text nombreJugador,tituloJugador;
    public InputField inputNombre;
    //Jugador jugador1,jugador2,jugador3,jugador4;
    public static List<Jugador> listaJugadores = new List<Jugador>();
    string mensajeAcceso = null;
    public Image alert;
    //public List<Image> gridJugadoes;
    public List<Image> imgFichas;
    public GameObject gridCantJugadores,gridJugadores;
    public Button btnJugar;

    public static int cantidadJugadores=2;
    int contadorJugadoreCreado=0;


    //Audio
    public AudioSource avisoAudioSource;
    public AudioClip avisoAudioClip;

    private void AvisoSonido()
    {
        avisoAudioSource.clip = avisoAudioClip;
        avisoAudioSource.Play();
    }

    public void SaberCantidadJugadores(int cantidad)
    {
        cantidadJugadores = cantidad;
        //for (int i=0;i< cantidadJugadores; i++)
        //{
        //    gridJugadoes[i].gameObject.SetActive(true);
        //}
        gridJugadores.SetActive(true);
        //btnJugar.gameObject.SetActive(true);
        gridCantJugadores.SetActive(false);
        

    }


    public void Click()
    {
        if (ValidarCampos(nombreJugador)!="Correcto")
        {
            AlertDialog(alert);
            AvisoSonido();
        }
        else
        {
            contadorJugadoreCreado++;
            if (contadorJugadoreCreado==cantidadJugadores)
            {
                CrearJugadores(nombreJugador, contadorJugadoreCreado - 1);
                GetComponent<OnClick>().SonidoBoton();
                SceneManager.LoadScene("Instrucciones");
            }
            else
            {
                CrearJugadores(nombreJugador,contadorJugadoreCreado-1);
                inputNombre.text = "";
                tituloJugador.text = "";
                tituloJugador.text ="Jugador"+(contadorJugadoreCreado+1);
                CambiarFicha(contadorJugadoreCreado-1,contadorJugadoreCreado);
                GetComponent<OnClick>().SonidoBoton();
            }
            
        }
        
        
    }
    // Start is called before the first frame update
    //void ValidarCampos(List<Text> CamposTxt)
    string ValidarCampos(Text CampoTxt)
    {
        string mensaje="";
        if (CampoTxt.text!="")
        {
            mensaje = "Correcto";
        }
        else
        {
            mensaje = "Debes escribir tu nombre";
        }
        mensajeAcceso = mensaje;
        return mensaje;
        //AlertDialog(alert);
        //CrearJugadores(CampoTxt,contadorJugadoreCreado-1);
    }


    void CambiarFicha(int indiceDesactivar,int indiceActivar){

        imgFichas[indiceDesactivar].gameObject.SetActive(false);
        imgFichas[indiceActivar].gameObject.SetActive(true);
    }

    void InstanciarJugadores(int indice)
    {
        if (indice == 0)
        {
            Jugador jugador1 = new Jugador();
            listaJugadores.Add(jugador1);
        }
        else if (indice == 1)
        {
            Jugador jugador2 = new Jugador();
            listaJugadores.Add(jugador2);
        }
        else if (indice == 2)
        {
            Jugador jugador3 = new Jugador();
            listaJugadores.Add(jugador3);
        }
        else if (indice == 3)
        {
            Jugador jugador4 = new Jugador();
            listaJugadores.Add(jugador4);
        }
    }
    public void CrearJugadores(Text nombreText,int i)
    {
        InstanciarJugadores(i);
        
        listaJugadores[i].NumeroJugador = i + 1;
        listaJugadores[i].Nombre = nombreText.text;
        listaJugadores[i].DineroActual = 100;
        listaJugadores[i].DineroGanado = 100;
        listaJugadores[i].PrestamoBancario = 0;
        listaJugadores[i].PrestamoBancarioDevolucionTotal = 0;
        listaJugadores[i].PrestamoSocio = 0;
        listaJugadores[i].PrestamoSocioDevolucionTotal = 0;
        listaJugadores[i].Turno = false;
        listaJugadores[i].Activado = true;
        listaJugadores[i].Posicion = null;
        listaJugadores[i].PosicionActual = 0;
        listaJugadores[i].NumeroTarjetasCliente = 0;
        listaJugadores[i].NumeroTarjetasPrestamoBanco = 0;
        listaJugadores[i].NumeroTarjetasPrestamoSocio = 0;
        listaJugadores[i].CantidadMaterial1 = 0;
        listaJugadores[i].CantidadMaterial2 = 0;
        listaJugadores[i].CantidadMaterial3 = 0;
        listaJugadores[i].CantidadMaterial4 = 0;
        listaJugadores[i].OrdenPuesto = 0;
        listaJugadores[i].Cartas = null;

        //Debug.Log(listaJugadores[i].NumeroJugador + " tiene nombre: " + listaJugadores[i].Nombre);
          
    }

    public void AlertDialog(Image AlertDialog)
    {

        if (mensajeAcceso != "Correcto")
        {
            AlertDialog.gameObject.SetActive(true);
            AlertDialog.GetComponentInChildren<Text>().text = mensajeAcceso;
        }
        else
        {
            SceneManager.LoadScene("SampleScene");
        }
    }


    void InstanciarCartas()
    {
        CartaProducto nombre =new CartaProducto();
    }
}
