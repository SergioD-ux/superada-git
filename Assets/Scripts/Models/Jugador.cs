﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts.Models
{
    public class Jugador
    {
        private int numeroJugador;
        private string nombre;
        private float dineroActual;
        private float dineroGanado;
        private float prestamoBancario;
        private float prestamoBancarioDevolucionTotal;
        private float prestamoSocio;
        private float prestamoSocioDevolucionTotal;
        private bool turno;
        private bool activado;
        private Transform posicion;

        private int posicionActual;
        private int numeroTarjetasCliente;
        private int numeroTarjetasPrestamoBanco;
        private int numeroTarjetasPrestamoSocio;
        private int cantidadMaterial1;
        private int cantidadMaterial2;
        private int cantidadMaterial3;
        private int cantidadMaterial4;
        private int ordenPuesto;

        private List<CartaProducto> cartas;

        public Jugador()
        {
        }

        public Jugador(int numeroJugador, string nombre, float dineroActual, float dineroGanado, float prestamoBancario, float prestamoBancarioDevolucionTotal, float prestamoSocio, float prestamoSocioDevolucionTotal, bool turno, bool activado, Transform posicion, int posicionActual, int numeroTarjetasCliente, int numeroTarjetasPrestamoBanco, int numeroTarjetasPrestamoSocio, int cantidadMaterial1, int cantidadMaterial2, int cantidadMaterial3, int cantidadMaterial4, int ordenPuesto, List<CartaProducto> cartas)
        {
            this.numeroJugador = numeroJugador;
            this.nombre = nombre;
            this.dineroActual = dineroActual;
            this.dineroGanado = dineroGanado;
            this.prestamoBancario = prestamoBancario;
            this.prestamoBancarioDevolucionTotal = prestamoBancarioDevolucionTotal;
            this.prestamoSocio = prestamoSocio;
            this.prestamoSocioDevolucionTotal = prestamoSocioDevolucionTotal;
            this.turno = turno;
            this.activado = activado;
            this.posicion = posicion;
            this.posicionActual = posicionActual;
            this.numeroTarjetasCliente = numeroTarjetasCliente;
            this.numeroTarjetasPrestamoBanco = numeroTarjetasPrestamoBanco;
            this.numeroTarjetasPrestamoSocio = numeroTarjetasPrestamoSocio;
            this.cantidadMaterial1 = cantidadMaterial1;
            this.cantidadMaterial2 = cantidadMaterial2;
            this.cantidadMaterial3 = cantidadMaterial3;
            this.cantidadMaterial4 = cantidadMaterial4;
            this.ordenPuesto = ordenPuesto;
            this.cartas = cartas;
        }

        public int NumeroJugador { get => numeroJugador; set => numeroJugador = value; }
        public string Nombre { get => nombre; set => nombre = value; }
        public float DineroActual { get => dineroActual; set => dineroActual = value; }
        public float DineroGanado { get => dineroGanado; set => dineroGanado = value; }
        public float PrestamoBancario { get => prestamoBancario; set => prestamoBancario = value; }
        public float PrestamoBancarioDevolucionTotal { get => prestamoBancarioDevolucionTotal; set => prestamoBancarioDevolucionTotal = value; }
        public float PrestamoSocio { get => prestamoSocio; set => prestamoSocio = value; }
        public float PrestamoSocioDevolucionTotal { get => prestamoSocioDevolucionTotal; set => prestamoSocioDevolucionTotal = value; }
        public bool Turno { get => turno; set => turno = value; }
        public bool Activado { get => activado; set => activado = value; }
        public Transform Posicion { get => posicion; set => posicion = value; }
        public int PosicionActual { get => posicionActual; set => posicionActual = value; }
        public int NumeroTarjetasCliente { get => numeroTarjetasCliente; set => numeroTarjetasCliente = value; }
        public int NumeroTarjetasPrestamoBanco { get => numeroTarjetasPrestamoBanco; set => numeroTarjetasPrestamoBanco = value; }
        public int NumeroTarjetasPrestamoSocio { get => numeroTarjetasPrestamoSocio; set => numeroTarjetasPrestamoSocio = value; }
        public int CantidadMaterial1 { get => cantidadMaterial1; set => cantidadMaterial1 = value; }
        public int CantidadMaterial2 { get => cantidadMaterial2; set => cantidadMaterial2 = value; }
        public int CantidadMaterial3 { get => cantidadMaterial3; set => cantidadMaterial3 = value; }
        public int CantidadMaterial4 { get => cantidadMaterial4; set => cantidadMaterial4 = value; }
        public int OrdenPuesto { get => ordenPuesto; set => ordenPuesto = value; }
        public List<CartaProducto> Cartas { get => cartas; set => cartas = value; }
    }
}
