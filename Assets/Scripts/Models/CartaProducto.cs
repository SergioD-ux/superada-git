﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts.Models
{
    public class CartaProducto
    {
        private int identificadorMateria;//10 al 70(de 10 en 10)
        private int identificadorIndice;//1 al 5  //Ejemplo: Una carta del primer producto con un indice 2 será 12
        private string nombre;
        private string materia_uno;
        private string materia_dos;
        private float monto;
        private bool ocupado;
        private bool disponible;

        public int IdentificadorMateria { get => identificadorMateria; set => identificadorMateria = value; }
        public int IdentificadorIndice { get => identificadorIndice; set => identificadorIndice = value; }
        public string Nombre { get => nombre; set => nombre = value; }
        public string Materia_uno { get => materia_uno; set => materia_uno = value; }
        public string Materia_dos { get => materia_dos; set => materia_dos = value; }
        public float Monto { get => monto; set => monto = value; }
        public bool Ocupado { get => ocupado; set => ocupado = value; }
        public bool Disponible { get => disponible; set => disponible = value; }

        public CartaProducto(int identificadorMateria, int identificadorIndice, string nombre, string materia_uno, string materia_dos, float monto, bool ocupado, bool disponible)
        {
            this.identificadorMateria = identificadorMateria;
            this.identificadorIndice = identificadorIndice;
            this.nombre = nombre;
            this.materia_uno = materia_uno;
            this.materia_dos = materia_dos;
            this.monto = monto;
            this.ocupado = ocupado;
            this.disponible = disponible;
        }

        public CartaProducto()
        {
        }
    }
}
